# Lab : Déployer des conteneurs auto-réparateurs sous Kubernetes


------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectif

Kubernetes propose plusieurs fonctionnalités qui peuvent être utilisées ensemble pour créer des applications d'auto-réparation dans divers scénarios. Dans cet atelier, vous pourrez mettre en pratique vos compétences en matière d'utilisation de fonctionnalités telles que les sondes et les politiques de redémarrage pour créer une application conteneur qui est automatiquement réparée lorsqu'elle cesse de fonctionner.

Nous allons : 

1. Définir une stratégie de redémarrage pour redémarrer le conteneur lorsqu'il est en panne

2. Créez une sonde de vivacité pour détecter quand l'application est tombée en panne

>![Alt text](img/image.png)


# Contexte

Vous travaillez pour BeeHive, une entreprise qui assure des expéditions régulières d'abeilles aux clients. L'entreprise est en train de déployer certaines applications sur Kubernetes qui gèrent le traitement des données liées à l'expédition.

L'un de ces composants d'application est un pod appelé `beebox-shipping-data` situé dans l' defaultespace de noms. Malheureusement, l'application exécutée dans ce pod plante à plusieurs reprises. Pendant que les développeurs cherchent pourquoi cette application plante, il vous a été demandé d'implémenter une solution d'auto-réparation dans Kubernetes pour récupérer rapidement chaque fois que l'application plante.

Heureusement, l'application peut être réparée lorsqu'elle plante simplement en redémarrant le conteneur. Modifiez la configuration du pod afin que l'application redémarre automatiquement en cas de panne. Vous pouvez détecter un crash d'application lorsque les demandes de portage `8080` sur le conteneur renvoient un `HTTP 500` code d'état.

Remarque : `kubeclt --export` ne fonctionne plus dans l'environnement de laboratoire. Une alternative serait :

```bash
kubectl get pod beebox-shipping-data -o yaml > beebox-shipping-data.yml
```

# Application

## 1. Connectez-vous au serveur de laboratoire fourni à l'aide des informations d'identification fournies :

```bash
ssh -i id_rsa user@PUBLIC_IP_ADDRESS_CONTROL_NODE
```

## 2. Définir une stratégie de redémarrage pour redémarrer le conteneur lorsqu'il est en panne
Recherchez le pod qui doit être modifié :

```bash
kubectl get pods -o wide
```
>![Alt text](img/image-1.png)
*Informations sur les pods en cours*


Prenez note de l' `beebox-shipping-data` adresse IP du pod, dans notre cas : `192.168.228.1`.


## 3. Vérification de l'état de fonctionnement du pod beebox-shipping-data

Utilisez le busyboxpod pour faire une requête au pod pour voir s'il fonctionne :

```bash
kubectl exec busybox -- curl 192.168.228.1:8080
```

>![Alt text](img/image-2.png)
*Réponse d'erreur du serveur*

Nous recevrons probablement un message d’erreur, nous pouvons deviner quelque chose ne marche pas même si le pod est en cours d'exécution.

Pour résoudre le problème, on pourrais simplement redemarrer le pod, mais nous allons opté pour une solution automatisée, nous allons commencer par vérifier la politique de redémarrage des pods.

## 4. Vérifier la politique de redémarrage ddes pods

Obtenir le descripteur YAML du pod :

```bash
kubectl get pod beebox-shipping-data -o yaml > beebox-shipping-data.yml
```

>![Alt text](img/image-3.png)
*Manifest de deployment du pod beebox-shipping-data*

Ouvrir le fichier :

```bash
nano beebox-shipping-data.yml
```

Contenu : 

```yaml
apiVersion: v1
kind: Pod
metadata:
  annotations:
    cni.projectcalico.org/containerID: 2599693b5f4f1d01871ad094fc1a9e4de35e4b56786b297812d74d621945b863
    cni.projectcalico.org/podIP: 192.168.228.1/32
    cni.projectcalico.org/podIPs: 192.168.228.1/32
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","kind":"Pod","metadata":{"annotations":{},"name":"beebox-shipping-data","namespace":"default"},"spec":{"containers":[{"image":"linuxacademycontent/random-crashing-web-server:1","name":"shipping-data"}],"restartPolicy":"Never"}}
  creationTimestamp: "2024-05-24T07:55:02Z"
  name: beebox-shipping-data
  namespace: default
  resourceVersion: "874"
  uid: 1f1c8704-b701-445e-a05e-e4327ac56d56
spec:
  containers:
  - image: linuxacademycontent/random-crashing-web-server:1
    imagePullPolicy: IfNotPresent
    name: shipping-data
    resources: {}
    terminationMessagePath: /dev/termination-log
    terminationMessagePolicy: File
    volumeMounts:
    - mountPath: /var/run/secrets/kubernetes.io/serviceaccount
      name: kube-api-access-njdct
      readOnly: true
  dnsPolicy: ClusterFirst
  enableServiceLinks: true
  nodeName: acgk8s-worker1
  preemptionPolicy: PreemptLowerPriority
  priority: 0
  restartPolicy: Never  # << Politique de redemarrage du pod
  schedulerName: default-scheduler
  securityContext: {}
  serviceAccount: default
  serviceAccountName: default
  terminationGracePeriodSeconds: 30
  tolerations:
  - effect: NoExecute
    key: node.kubernetes.io/not-ready
    operator: Exists
    tolerationSeconds: 300
  - effect: NoExecute
    key: node.kubernetes.io/unreachable
    operator: Exists
    tolerationSeconds: 300
  volumes:
  - name: kube-api-access-njdct
    projected:
      defaultMode: 420
      sources:
      - serviceAccountToken:
          expirationSeconds: 3607
          path: token
      - configMap:
          items:
          - key: ca.crt
            path: ca.crt
          name: kube-root-ca.crt
      - downwardAPI:
          items:
          - fieldRef:
              apiVersion: v1
              fieldPath: metadata.namespace
            path: namespace
status:
  conditions:
  - lastProbeTime: null
    lastTransitionTime: "2024-05-24T07:56:08Z"
    status: "True"
    type: Initialized
  - lastProbeTime: null
    lastTransitionTime: "2024-05-24T07:56:44Z"
    status: "True"
    type: Ready
  - lastProbeTime: null
    lastTransitionTime: "2024-05-24T07:56:44Z"
    status: "True"
    type: ContainersReady
  - lastProbeTime: null
    lastTransitionTime: "2024-05-24T07:56:08Z"
    status: "True"
    type: PodScheduled
  containerStatuses:
  - containerID: containerd://3f7a1eab20d276e305daacce962f6ecbe05332c1d810bf15d7214e5b76269c83
    image: docker.io/linuxacademycontent/random-crashing-web-server:1
    imageID: docker.io/linuxacademycontent/random-crashing-web-server@sha256:961fb2f1241af9bd1de39d62aaac28a55fa0ec215b86a0c1f67c49a13cbcd0e2
    lastState: {}
    name: shipping-data
    ready: true
    restartCount: 0
    started: true
    state:
      running:
        startedAt: "2024-05-24T07:56:44Z"
  hostIP: 10.0.1.102
  phase: Running
  podIP: 192.168.228.1
  podIPs:
  - ip: 192.168.228.1
  qosClass: BestEffort
  startTime: "2024-05-24T07:56:08Z"
```

l'on peut constater que la polique de redemarrage est à **"Never"**

Réglez le **restartPolicy** sur **Always** :

```yaml
spec:
  ...
  restartPolicy: Always
  ...
```
Créez une sonde de vivacité pour détecter quand l'application est tombée en panne
Ajoutez une sonde d'activité :

```yaml
spec:
  containers:
  - ...
    name: shipping-data
    livenessProbe: #<< Sonde de vivacité
      httpGet: #<< Type de requete à exécuté par k8s dans le pod
        path: / #<< repertoire où sera exécuté la requette
        port: 8080 #<< port à utiliser pour la requette
      initialDelaySeconds: 5 #<< Delai initial d'attente avant le première requette
      periodSeconds: 5 #<< delai de repétition de la requette
    ...
```
Enregistrez et quittez le fichier en appuyant sur Échap suivi de :wq.

## 5. Supprimez le pod existant :

```bash
kubectl delete pod beebox-shipping-data
```

## 6. Recréez le pod en appliquant les modifications :

```bash
kubectl apply -f beebox-shipping-data.yml
```

>![Alt text](img/image-4.png)
*Création du pod réussie*

## 7. Vérifier l'état du pod crée

```bash
kubectl get pods -o wide
```

>![Alt text](img/image-5.png)
*Informations sur les pods*

Si l'on attend environ une minute et vérifiez à nouveau, l'on devrais voir que le pod a redémarré chaque fois que l'application plante.

## 8. Vérifiez à nouveau la réponse http du pod

il aura une nouvelle adresse IP  :

```bash
kubectl exec busybox -- curl 192.168.228.3:8080
```

>![Alt text](img/image-6.png)

Comme visible sur l'image, le pod à craché et redemarré plusieurs fois et on fini par obtenir une reponse positive du serveur web.
